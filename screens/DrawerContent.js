import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, ToastAndroid, Image} from 'react-native';
import {ListItem, Icon} from 'react-native-elements';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import * as Keychain from 'react-native-keychain';
import {AuthContext} from './Context';
import axios from 'axios';

const DrawerContent = (props) => {
  const [userId, setUserId] = useState('');
  const [photoProfile, setPhotoProfile] = useState(null);
  const [name, setName] = useState('');

  const {signOut} = React.useContext(AuthContext);

  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    try {
      const credentials = await Keychain.getGenericPassword();
      const id = credentials.username;
      const res = await axios.get(
        `https://be-mini-project.herokuapp.com/api/profile/${id}`,
        {
          headers: {
            Authorization: credentials.password,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data;
        console.log(data);
        setUserId(data.id);
        setName(data.name);
        setPhotoProfile(data.picture);
      }
    } catch (e) {
      const error = e.response.data.message;
      ToastAndroid.show(error, ToastAndroid.SHORT);
    }
  };

  const handleLogOut = async () => {
    try {
      // await AsyncStorage.clear();
      await Keychain.resetGenericPassword();
      signOut();
    } catch (e) {
      // clear error
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image source={{uri: photoProfile}} style={styles.image} />
        <Text style={styles.name}>{name}</Text>
      </View>
      <DrawerContentScrollView {...props} style={styles.content}>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
      <ListItem
        title={<Text style={styles.title}>Log Out</Text>}
        leftIcon={
          <Icon
            name="exit-to-app"
            type="materialicons"
            size={25}
            color="#2d3436"
          />
        }
        bottomDivider
        onPress={handleLogOut}
      />
    </View>
  );
};

export default DrawerContent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#2f80ed',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 200,
  },
  content: {
    paddingVertical: 10,
  },
  title: {
    textAlign: 'center',
    paddingRight: 20,
    letterSpacing: 2,
  },
  image: {
    height: 110,
    width: 110,
    position: 'relative',
    borderRadius: 100,
    resizeMode: 'cover',
    marginBottom: 7,
  },
  name: {
    color: '#fff',
    fontSize: 14,
    letterSpacing: 2,
  },
});
