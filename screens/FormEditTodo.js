import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Platform,
  Button,
  TouchableOpacity,
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

const FormEditTodo = ({
  dataId,
  dataTitle,
  dataDescription,
  dataDueDate,
  editTodo,
  fdueDate,
}) => {
  const [date, setDate] = useState(new Date(dataDueDate));
  const [show, setShow] = useState(false);
  const [dueDate, setDueDate] = useState(fdueDate);
  const [title, setTitle] = useState(dataTitle);
  const [description, setDescription] = useState(dataDescription);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    const dates = currentDate.getDate();
    const month = currentDate.getMonth();
    const year = currentDate.getFullYear();
    setDueDate(`${month + 1}/${dates}/${year}`);
  };

  const showDatepicker = () => {
    setShow(true);
  };

  const handleTitle = (val) => {
    setTitle(val);
  };

  const handleDescription = (val) => {
    setDescription(val);
  };

  return (
    <View>
      <Text style={styles.titleModal}>Edit Todo</Text>
      <View style={styles.action}>
        <Text style={styles.actionLabel}>Name</Text>
        <View style={styles.actionBody}>
          <TextInput
            placeholder="Enter Title"
            autoCapitalize="none"
            maxLength={24}
            style={styles.textInput}
            defaultValue={dataTitle}
            onChangeText={(val) => handleTitle(val)}
          />
        </View>
      </View>
      <View style={styles.action}>
        <Text style={styles.actionLabel}>Description</Text>
        <View style={styles.actionBody}>
          <TextInput
            placeholder="Enter Description"
            autoCapitalize="none"
            multiline={true}
            numberOfLines={3}
            style={styles.textInput}
            defaultValue={dataDescription}
            onChangeText={(val) => handleDescription(val)}
          />
        </View>
      </View>
      <View style={styles.date}>
        <TouchableOpacity onPress={showDatepicker} style={styles.btnDatePicker}>
          <Text style={styles.btnText}>Pick Due Date</Text>
        </TouchableOpacity>
        <View style={styles.actionDate}>
          <Text style={styles.dateText}>{dueDate}</Text>
        </View>
      </View>
      {show && (
        <DateTimePicker
          timeZoneOffsetInMinutes={0}
          value={date}
          mode="date"
          onChange={onChange}
        />
      )}
      <View style={styles.btnContainer}>
        <TouchableOpacity
          style={styles.btnSubmit}
          onPress={() => editTodo(dataId, title, description, dueDate)}>
          <Text style={styles.btnText}>Edit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default FormEditTodo;

const styles = StyleSheet.create({
  action: {
    flexDirection: 'column',
    marginTop: 15,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: 'grey',
    paddingTop: 15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  date: {
    flexDirection: 'row',
  },
  actionDate: {
    borderWidth: 1,
    borderRadius: 30,
    width: '45%',
    paddingVertical: 10,
    marginTop: 20,
    marginHorizontal: 10,
  },
  dateText: {
    alignSelf: 'center',
  },
  actionLabel: {
    marginTop: -5,
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  actionBody: {
    flexDirection: 'row',
    borderColor: 'grey',
    paddingTop: 7,
  },
  textInput: {
    flex: 1,
    marginTop: -12,
    paddingLeft: 12,
    color: '#05375a',
  },
  titleModal: {
    alignSelf: 'center',
    fontSize: 25,
    paddingBottom: 10,
  },
  btnDatePicker: {
    backgroundColor: '#2F80ED',
    paddingVertical: 10,
    marginTop: 20,
    borderRadius: 30,
    width: '50%',
  },
  btnText: {
    alignSelf: 'center',
    color: '#fff',
  },
  btnSubmit: {
    backgroundColor: '#2F80ED',
    paddingVertical: 10,
    marginTop: 20,
    borderRadius: 30,
  },
});
