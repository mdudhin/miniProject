import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ToastAndroid,
  SafeAreaView,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import * as Keychain from 'react-native-keychain';
import TodoImage from '../TodoImage';
import Todo from '../Todo';
import axios from 'axios';
import {Icon} from 'react-native-elements';
import FormAddTodo from '../FormAddTodo';
import Modal from 'react-native-modal';

const Home = ({navigation}) => {
  const [userToken, setUserToken] = useState(null);
  const [email, setEmail] = useState('');
  const [todayDate, setTodayDate] = useState('');
  const [todo, setTodo] = useState([]);
  const [isModalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    getDate();
  }, []);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const getData = async () => {
    try {
      const credentials = await Keychain.getGenericPassword();
      setEmail(credentials.username);
      setUserToken(credentials.password);
      const res = await axios.get(
        'https://be-mini-project.herokuapp.com/api/task/',
        {
          headers: {
            Authorization: credentials.password,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data.Search_Result;
        setTodo(data);
      }
    } catch (e) {
      const error = e.response.data.message;
      ToastAndroid.show(error, ToastAndroid.SHORT);
    }
  };

  const addTodo = async (title, description, dueDate) => {
    setModalVisible(false);
    const credentials = await Keychain.getGenericPassword();
    console.log(title, description, dueDate);
    axios
      .post(
        'https://be-mini-project.herokuapp.com/api/task/',
        {
          title: title,
          description: description,
          due_date: dueDate,
        },
        {
          headers: {
            Authorization: credentials.password,
          },
        },
      )
      .then((res) => {
        if (res.status == 201) {
          getData();
          ToastAndroid.show('Success to add todo !', ToastAndroid.SHORT);
        } else {
          getData();
          ToastAndroid.show('Failed to add todo !', ToastAndroid.SHORT);
        }
      });
  };

  const todoEdit = async (id, title, description, dueDate) => {
    setModalVisible(false);
    const credentials = await Keychain.getGenericPassword();
    console.log(id, title, description, dueDate);
    todo.map((item) => {
      if (item.id === id) {
        console.log(item.id, 'ss', item.completion);
        axios
          .put(
            `https://be-mini-project.herokuapp.com/api/task/${id}`,
            {
              title: title,
              description: description,
              due_date: dueDate,
            },
            {
              headers: {
                Authorization: credentials.password,
              },
            },
          )
          .then((res) => {
            if (res.status == 200) {
              getData();
              ToastAndroid.show('Success to edit todo !', ToastAndroid.SHORT);
            } else {
              getData();
              ToastAndroid.show('Failed to edit todo !', ToastAndroid.SHORT);
            }
          });
      }
      return item;
    });
  };

  const todoComplete = async (id) => {
    try {
      const credentials = await Keychain.getGenericPassword();
      todo.map((item) => {
        if (item.id === id) {
          console.log(item.id, 'ss', item.completion);
          axios
            .put(
              `https://be-mini-project.herokuapp.com/api/task/${id}`,
              {
                completion: !item.completion,
              },
              {
                headers: {
                  Authorization: credentials.password,
                },
              },
            )
            .then((res) => {
              if (res.status == 200) {
                if (item.completion) {
                  getData();
                  ToastAndroid.show(
                    'Success to un complete todo!',
                    ToastAndroid.SHORT,
                  );
                } else {
                  getData();
                  ToastAndroid.show(
                    'Success to complete todo!',
                    ToastAndroid.SHORT,
                  );
                }
              } else {
                getData();
                ToastAndroid.show(
                  'Failed to complete todo!',
                  ToastAndroid.SHORT,
                );
              }
            });
        }
        return item;
      });
    } catch (e) {
      console.log(e);
    }
  };

  const todoImportance = async (id) => {
    try {
      const credentials = await Keychain.getGenericPassword();
      todo.map((item) => {
        if (item.id === id) {
          console.log(item.id, 'ss', item.importance);
          axios
            .put(
              `https://be-mini-project.herokuapp.com/api/task/${id}`,
              {
                importance: !item.importance,
              },
              {
                headers: {
                  Authorization: credentials.password,
                },
              },
            )
            .then((res) => {
              if (res.status == 200) {
                if (item.importance) {
                  getData();
                  ToastAndroid.show(
                    'Success delete todo from importance!',
                    ToastAndroid.SHORT,
                  );
                } else {
                  getData();
                  ToastAndroid.show(
                    'Success add todo to importance!',
                    ToastAndroid.SHORT,
                  );
                }
              } else {
                getData();
                ToastAndroid.show(
                  'Failed add todo to importance!',
                  ToastAndroid.SHORT,
                );
              }
            });
        }
        return item;
      });
    } catch (e) {
      console.log(e);
    }
  };

  const todoDelete = async (id) => {
    try {
      const credentials = await Keychain.getGenericPassword();
      axios
        .delete(`https://be-mini-project.herokuapp.com/api/task/${id}`, {
          headers: {
            Authorization: credentials.password,
          },
        })
        .then((res) => {
          if (res.status == 200) {
            getData();
            ToastAndroid.show('Success to delete!', ToastAndroid.SHORT);
          } else {
            getData();
            ToastAndroid.show('Failed to delete!', ToastAndroid.SHORT);
          }
        });
    } catch (e) {
      console.log(e);
    }
  };

  const getDate = () => {
    const currentDate = new Date();
    const day = currentDate.getDay();
    const date = currentDate.getDate();
    const month = currentDate.getMonth();
    const year = currentDate.getFullYear();
    const dList = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];
    const mList = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    setTodayDate(`${dList[day]}, ${date} ${mList[month]} ${year}`);
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#0055ba" />
      <View style={styles.header}>
        <TodoImage />
        <View style={styles.headerContent}>
          <Text style={styles.title}>My Day</Text>
          <Text style={styles.date}>{todayDate}</Text>
        </View>
      </View>
      <View style={styles.content}>
        <Todo
          todo={todo}
          getData={getData}
          todoComplete={todoComplete}
          todoImportance={todoImportance}
          todoDelete={todoDelete}
          todoEdit={todoEdit}
        />
      </View>
      <TouchableWithoutFeedback onPress={toggleModal}>
        <View style={styles.floatingButton}>
          <Icon name="plus" type="feather" size={30} color="#fff" />
        </View>
      </TouchableWithoutFeedback>
      <Modal isVisible={isModalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.addModal}>
            <TouchableWithoutFeedback onPress={toggleModal}>
              <View style={styles.closeBtn}>
                <Icon name="x" type="feather" size={30} />
              </View>
            </TouchableWithoutFeedback>
            <FormAddTodo addTodo={addTodo} />
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e0e0e0',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingHorizontal: 20,
    paddingBottom: 30,
    backgroundColor: '#2f80ed',
  },
  content: {
    flex: 1,
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  headerContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    marginTop: 15,
    marginBottom: -10,
  },
  title: {
    fontSize: 20,
    letterSpacing: 2,
    color: '#fff',
    paddingHorizontal: 5,
  },
  date: {
    fontSize: 13,
    letterSpacing: 2,
    color: '#fff',
    paddingHorizontal: 5,
    paddingTop: 7,
  },
  floatingButton: {
    position: 'absolute',
    backgroundColor: '#2f80ed',
    justifyContent: 'center',
    width: 60,
    height: 60,
    borderRadius: 30,
    right: 20,
    bottom: 20,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 50,
  },
  addModal: {
    backgroundColor: '#fff',
    margin: 20,
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  closeBtn: {
    alignSelf: 'flex-end',
    width: 40,
    height: 40,
    marginTop: -20,
  },
});
