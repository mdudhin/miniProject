import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  ToastAndroid,
  KeyboardAvoidingView,
} from 'react-native';
import TodoImage from '../TodoImage';
import LoginForm from './components/LoginForm';
import LoginFooter from './components/LoginFooter';
// import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {AuthContext} from '../Context';
import * as Keychain from 'react-native-keychain';

const Login = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [wrongEmail, setWrongEmail] = useState(false);
  const [trueEmail, setTrueEmail] = useState(false);
  const [wrongEmailStatement, setWrongEmailStatement] = useState(false);

  const [password, setPassword] = useState('');
  const [wrongPassword, setWrongPassword] = useState(false);
  const [wrongPasswordStatement, setWrongPasswordStatement] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const {signIn} = React.useContext(AuthContext);

  const handleEmail = (val) => {
    setEmail(val);

    if (email != '') {
      setWrongEmail(false);
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (regex.test(email)) {
      console.log('regex', regex.test(email));
      setWrongEmail(false);
      setTrueEmail(true);
    } else {
      console.log('regex gagal', regex.test(email));
      setWrongEmail(true);
      setWrongEmailStatement('Wrong email format');
    }
  };

  const handlePassword = (val) => {
    setPassword(val);
    if (password != '') {
      setWrongPassword(false);
    }
  };

  const updateSecureTextEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const login = async () => {
    if (email == '') {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Email must be filled');
    } else {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    }

    if (password == '') {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be filled');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email != '' && password != '' && regex.test(email)) {
      console.log('benar', email, password);
      setIsLoading(true);
      try {
        const apiLogin = await axios({
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
          },
          url: 'https://be-mini-project.herokuapp.com/api/user/login',
          data: JSON.stringify({
            email,
            password,
          }),
        });
        const token = apiLogin.data.data.token;
        console.log(apiLogin.data.data.user.id);
        const userId = `${apiLogin.data.data.user.id}`;
        setIsLoading(false);
        if (token) {
          // const username = email;
          // const password = data.Token
          await Keychain.setGenericPassword(userId, token);
          // await AsyncStorage.setItem('@token', data.Token);
          signIn(token);
        }
      } catch (e) {
        setIsLoading(false);
        const error = e.response.data.message;
        ToastAndroid.show(error, ToastAndroid.SHORT);
      }
    } else {
      console.log('error');
    }
  };

  return (
    <KeyboardAvoidingView behavior="height" style={styles.container}>
      <View style={styles.header}>
        <TodoImage />
      </View>
      <View style={styles.content}>
        <LoginForm
          navigation={navigation}
          email={email}
          wrongEmail={wrongEmail}
          trueEmail={trueEmail}
          wrongEmailStatement={wrongEmailStatement}
          password={password}
          wrongPassword={wrongPassword}
          wrongPasswordStatement={wrongPasswordStatement}
          secureTextEntry={secureTextEntry}
          handleEmail={handleEmail}
          handlePassword={handlePassword}
          updateSecureTextEntry={updateSecureTextEntry}
          login={login}
          isLoading={isLoading}
        />
      </View>
      <View style={styles.footer}>
        <LoginFooter />
      </View>
    </KeyboardAvoidingView>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#2F80ED',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingHorizontal: 20,
    paddingBottom: 30,
  },
  content: {
    backgroundColor: '#fff',
    borderRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
    marginLeft: 20,
    marginRight: 20,
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
});
