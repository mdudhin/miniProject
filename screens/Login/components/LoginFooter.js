import React from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';

const LoginFooter = () => {
  return (
    <View>
      <Text style={styles.text}>or login with</Text>
      <View style={styles.action}>
        <TouchableOpacity style={styles.icon}>
          <Icon name="facebook" type="font-awesome" size={20} color="white" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.icon}>
          <Icon
            name="google-plus"
            type="font-awesome"
            size={20}
            color="white"
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.icon}>
          <Icon name="linkedin" type="font-awesome" size={20} color="white" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginFooter;

const styles = StyleSheet.create({
  text: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
  },
  action: {
    flexDirection: 'row',
    alignSelf: 'center',
    paddingVertical: 30,
  },
  icon: {
    borderWidth: 1,
    borderRadius: 50,
    borderColor: '#fff',
    width: 50,
    height: 50,
    justifyContent: 'center',
    marginHorizontal: 10,
  },
});
