import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  ToastAndroid,
  Alert,
} from 'react-native';
import PhotoProfile from './components/PhotoProfile';
import ProfileForm from './components/ProfileForm';
import ImagePicker from 'react-native-image-picker';
import * as Keychain from 'react-native-keychain';
import axios from 'axios';

const Profile = ({navigation}) => {
  const [userId, setUserId] = useState('');
  const [photoProfile, setPhotoProfile] = useState(null);
  const [photoName, setPhotoName] = useState(null);
  const [photoPath, setPhotoPath] = useState(null);

  const [name, setName] = useState('');
  const [wrongName, setWrongName] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    try {
      const credentials = await Keychain.getGenericPassword();
      const id = credentials.username;
      const res = await axios.get(
        `https://be-mini-project.herokuapp.com/api/profile/${id}`,
        {
          headers: {
            Authorization: credentials.password,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data;
        console.log(data);
        setUserId(data.id);
        setName(data.name);
        setPhotoProfile(data.picture);
      }
    } catch (e) {
      const error = e.response.data.message;
      ToastAndroid.show(error, ToastAndroid.SHORT);
    }
  };

  const handleChoosePhoto = () => {
    const options = {
      title: 'Select Photo',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('Image picker error = ', response.error);
      } else {
        setPhotoProfile(response.uri);
        setPhotoName(response.fileName);
        setPhotoPath(response);
        // uploadImage();
      }
    });
  };

  // const uploadImage = async () => {
  //   console.log('upload', photoPath.uri, photoPath.fileName);
  //   const dataForm = new FormData();
  //   dataForm.append('image', {
  //     uri: photoProfile,
  //     name: photoName,
  //     type: 'image/jpg',
  //   });
  //   const credentials = await Keychain.getGenericPassword();
  //   try {
  //     await axios.put(
  //       `https://be-mini-project.herokuapp.com/api/profile/${userId}`,
  //       dataForm,
  //       {
  //         headers: {
  //           authorization: credentials.password,
  //         },
  //       },
  //     );
  //   } catch (e) {
  //     console.log(e.response);
  //   }
  // };

  const handleName = (val) => {
    setName(val);
    if (name != '') {
      setWrongName(false);
    }
  };

  const updateProfile = async (id, profileName, picture) => {
    const credentials = await Keychain.getGenericPassword();
    console.log(id, photoPath.type, photoPath.data);
    setIsLoading(true);
    const data = new FormData();
    data.append('name', profileName);
    data.append('image', {
      uri: photoProfile,
      name: photoName,
      type: 'image/jpg',
    });
    try {
      await axios
        .put(`https://be-mini-project.herokuapp.com/api/profile/${id}`, data, {
          headers: {
            authorization: credentials.password,
            'Content-Type': 'application/json',
          },
        })
        .then((res) => {
          setIsLoading(false);
          if (res.status == 200) {
            Alert.alert(
              'Success!',
              'Success to update profile!',
              [{text: 'OK', onPress: () => getUser()}],
              {cancelable: false},
            );
          } else {
            Alert.alert(
              'Failed!',
              'Failed to update profile!',
              [{text: 'OK', onPress: () => getUser()}],
              {cancelable: false},
            );
          }
        });
    } catch (e) {
      console.log(e);
      setIsLoading(false);
    }
    // axios
    //   .put(
    //     `https://be-mini-project.herokuapp.com/api/profile/${id}`,
    //     data,
    //     {
    //       headers: {
    //         authorization: credentials.password,
    //       },
    //     },
    //   )
    // .then((res) => {
    //   setIsLoading(false);
    //   if (res.status == 200) {
    //     Alert.alert(
    //       'Success!',
    //       'Success to update profile!',
    //       [{text: 'OK', onPress: () => getUser()}],
    //       {cancelable: false},
    //     );
    //   } else {
    //     Alert.alert(
    //       'Failed!',
    //       'Failed to update profile!',
    //       [{text: 'OK', onPress: () => getUser()}],
    //       {cancelable: false},
    //     );
    //   }
    // });
  };

  return (
    <KeyboardAvoidingView style={styles.container} behavior="height">
      <View style={styles.header}>
        <PhotoProfile
          photoProfile={photoProfile}
          handleChoosePhoto={handleChoosePhoto}
        />
      </View>
      <View style={styles.content}>
        <ProfileForm
          userId={userId}
          name={name}
          photoProfile={photoProfile}
          wrongName={wrongName}
          isLoading={isLoading}
          handleName={handleName}
          updateProfile={updateProfile}
        />
      </View>
      <View style={styles.footer} />
    </KeyboardAvoidingView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e0e0e0',
    justifyContent: 'center',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 50,
    paddingHorizontal: 20,
    paddingBottom: 30,
  },
  content: {
    paddingHorizontal: 20,
    paddingVertical: 30,
    marginLeft: 20,
    marginRight: 20,
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
});
