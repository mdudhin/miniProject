import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  ToastAndroid,
  Alert,
} from 'react-native';
import RegisterForm from './components/RegisterForm';
import TodoImage from '../TodoImage';
import axios from 'axios';

const Register = ({navigation}) => {
  const [name, setName] = useState('');
  const [wrongName, setWrongName] = useState(false);
  const [trueName, setTrueName] = useState(false);

  const [email, setEmail] = useState('');
  const [wrongEmail, setWrongEmail] = useState(false);
  const [trueEmail, setTrueEmail] = useState(false);
  const [wrongEmailStatement, setWrongEmailStatement] = useState(false);

  const [password, setPassword] = useState('');
  const [wrongPassword, setWrongPassword] = useState(false);
  const [wrongPasswordStatement, setWrongPasswordStatement] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const handleName = (val) => {
    setName(val);
    if (name != '') {
      setWrongName(false);
      setTrueName(true);
    }
  };

  const handleEmail = (val) => {
    setEmail(val);

    if (email != '') {
      setWrongEmail(false);
      setTrueEmail(true);
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (regex.test(email)) {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    } else {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Wrong email format');
    }
  };

  const handlePassword = (val) => {
    setPassword(val);

    if (password != '') {
      setWrongPassword(false);
    }

    const passwordLength = /^(?=.{6})/;
    const passwordNumber = /^(?=.*[0-9])/;

    if (!passwordLength.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 6 character');
    } else if (!passwordNumber.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 1 numeric');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }
  };

  const updateSecureTextEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const register = async () => {
    if (name == '') {
      setWrongName(true);
      setTrueName(false);
    } else {
      setWrongName(false);
      setTrueEmail(true);
    }

    if (email == '') {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Email must be filled');
    } else {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    }

    if (password == '') {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be filled');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const passwordLength = /^(?=.{6})/;
    const passwordNumber = /^(?=.*[0-9])/;

    if (regex.test(email)) {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    } else {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Wrong email format');
    }

    if (!passwordLength.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 6 character');
    } else if (!passwordNumber.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 1 numeric');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }

    if (
      name != '' &&
      email != '' &&
      password != '' &&
      regex.test(email) &&
      passwordLength.test(password) &&
      passwordNumber.test(password)
    ) {
      console.log('Register berhasil', name, email, password);
      try {
        const apiRegister = await axios({
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
          },
          url: 'https://be-mini-project.herokuapp.com/api/user/register',
          data: JSON.stringify({
            email,
            password,
            name,
          }),
        });
        console.log(apiRegister.data);
        if (apiRegister.data) {
          Alert.alert(
            'Success!',
            'Success to register',
            [{text: 'OK', onPress: () => navigation.navigate('Login')}],
            {cancelable: false},
          );
        }
      } catch (e) {
        const error = e.response.data.message;
        // ToastAndroid.show(error, ToastAndroid.SHORT);
        Alert.alert(
          'Failed!',
          error,
          [{text: 'OK', onPress: () => console.log(error)}],
          {cancelable: false},
        );
      }
    } else {
      console.log('Register gagal');
    }
  };

  return (
    <KeyboardAvoidingView style={styles.container} behavior="height">
      <View style={styles.header}>
        <TodoImage />
      </View>
      <View style={styles.content}>
        <RegisterForm
          navigation={navigation}
          name={name}
          wrongName={wrongName}
          trueName={trueName}
          email={email}
          wrongEmail={wrongEmail}
          trueEmail={trueEmail}
          wrongEmailStatement={wrongEmailStatement}
          password={password}
          wrongPassword={wrongPassword}
          wrongPasswordStatement={wrongPasswordStatement}
          secureTextEntry={secureTextEntry}
          handleName={handleName}
          handleEmail={handleEmail}
          handlePassword={handlePassword}
          updateSecureTextEntry={updateSecureTextEntry}
          register={register}
        />
      </View>
      <View style={styles.footer} />
    </KeyboardAvoidingView>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2F80ED',
    justifyContent: 'center',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 50,
    paddingHorizontal: 20,
    paddingBottom: 30,
  },
  content: {
    backgroundColor: '#fff',
    borderRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
    marginLeft: 20,
    marginRight: 20,
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
});
