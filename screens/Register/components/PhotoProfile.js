import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

const PhotoProfile = ({loadingImage, photoProfile, handleChoosePhoto}) => {
  return (
    <View>
      <Image
        source={
          loadingImage
            ? {
                uri: photoProfile,
              }
            : require('../../../assets/images/defaultImage.png')
        }
        style={styles.image}
      />
      <TouchableOpacity style={styles.btn} onPress={handleChoosePhoto}>
        <Text style={styles.imageText}>Pick Image</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PhotoProfile;

const styles = StyleSheet.create({
  btn: {
    paddingTop: 15,
  },
  imageText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
  image: {
    height: 130,
    width: 130,
    position: 'relative',
    borderRadius: 100,
    resizeMode: 'cover',
  },
});
