import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import TodoImage from './TodoImage';

const Splash = () => {
  return (
    <View style={styles.container}>
      <TodoImage />
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2F80ED',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
