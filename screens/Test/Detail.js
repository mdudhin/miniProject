import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {connect} from 'react-redux';

class Detail extends Component {
  render() {
    return (
      <View>
        <Text> Name : {this.props.name} </Text>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  name: state.auth.name,
});

export default connect(mapStateToProps, null)(Detail);
// import React from 'react';
// import {StyleSheet, Text, View} from 'react-native';
// import {connect} from 'react-redux';

// const Detail = ({name}) => {
//   return (
//     <View>
//       <Text>{name}</Text>
//     </View>
//   );
// };

// const mapStateToProps = (state) => ({
//   name: state.auth.name,
// });

// export default connect(mapStateToProps, null)(Detail);
