import React from 'react';
import {StyleSheet, SafeAreaView, FlatList} from 'react-native';
import Item from './Item';

const Todo = ({
  getData,
  todo,
  todoComplete,
  todoImportance,
  todoDelete,
  todoEdit,
}) => {
  return (
    <FlatList
      data={todo}
      renderItem={({item}) => (
        <Item
          id={item.id}
          title={item.title}
          description={item.description}
          dueDate={item.due_date}
          completion={item.completion}
          importance={item.importance}
          todoComplete={todoComplete}
          todoImportance={todoImportance}
          todoDelete={todoDelete}
          todoEdit={todoEdit}
        />
      )}
      keyExtractor={(item) => `${item.id}`}
      refreshing={false}
      onRefresh={getData}
    />
  );
};

export default Todo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
