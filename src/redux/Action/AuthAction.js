export const save_data = (data) => {
  return {
    type: 'SAVE_DATA',
    payload: data,
  };
};

export const change_name = (params) => {
  return {
    type: 'CHANGE_NAME',
    payload: params,
  };
};
