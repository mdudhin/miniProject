const initialState = {
  user: {},
  data: '',
  name: 'Mdudhin',
  description: '',
};

export const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {...state, user: action.payload, description: action.description};
    case 'ERROR':
      return {...state, description: action.payload};
    case 'SAVE_DATA':
      return {...state, data: action.payload};
    case 'CHANGE_NAME':
      return {...state, name: action.payload};
    default:
      return state;
  }
};
