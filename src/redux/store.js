import reducer from './Reducer/AppReducer';
import {createStore} from 'redux';

export const store = createStore(reducer);
